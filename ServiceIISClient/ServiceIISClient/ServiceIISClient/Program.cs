﻿using ServiceIISClient.WorkingTaskService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ServiceIISClient
{
    public class Program
    {
        static void Main(string[] args)
        {
            IWorkingTaskContract channel = new ChannelFactory<IWorkingTaskContract>("BasicHttpBinding_IWorkingTaskContract").CreateChannel();

            Console.WriteLine("Press Enter to get workingTasks.");
                do
                {
                    var pressedKey = Console.ReadKey();
                    if (pressedKey.Key == ConsoleKey.Enter)
                    {
                    try
                    {

                        Console.WriteLine("Request processing...");

                        bool serviceIsOnline = channel.ServiceStatusTestCheck();
                        Console.WriteLine($"Server status: {serviceIsOnline}");

                        //var test3 = channel.TestOperationGetTypes();

                        //var test = channel.CollectionTest();
                        //List<WorkingTask> test2 = channel.GetAllActiveTasks("second");

                        //string mainTasks = string.Join(", ", channel.GetAllActiveTasks("main").Select(task => task.Name).ToList());
                        //Console.WriteLine($"MainDB values: {mainTasks}");

                        //string secondTasks = string.Join(", ", channel.GetAllActiveTasks("second").Select(task => task.Name).ToList());
                        //Console.WriteLine($"SecondDB values: {secondTasks}");

                    }
                    catch (FaultException fe)
                    {
                        Console.WriteLine("FAILED!");
                    }
                    catch (CommunicationException ce)
                    {
                        Console.WriteLine("FAILED!\n" + ce.Message);
                    }
}

                }
                while (true);

        }
    }
}
