﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServiceIISClient.WorkingTaskService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="WorkingTask", Namespace="http://schemas.datacontract.org/2004/07/WcfDal.Models")]
    [System.SerializableAttribute()]
    public partial class WorkingTask : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="WorkingTaskType", Namespace="http://schemas.datacontract.org/2004/07/WcfDal.Models")]
    [System.SerializableAttribute()]
    public partial class WorkingTaskType : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Code {
            get {
                return this.CodeField;
            }
            set {
                if ((object.ReferenceEquals(this.CodeField, value) != true)) {
                    this.CodeField = value;
                    this.RaisePropertyChanged("Code");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="WorkingTaskService.IWorkingTaskContract")]
    public interface IWorkingTaskContract {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWorkingTaskContract/GetAllTasks", ReplyAction="http://tempuri.org/IWorkingTaskContract/GetAllTasksResponse")]
        System.Collections.Generic.List<ServiceIISClient.WorkingTaskService.WorkingTask> GetAllTasks(string databasePrefix);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWorkingTaskContract/GetAllActiveTasks", ReplyAction="http://tempuri.org/IWorkingTaskContract/GetAllActiveTasksResponse")]
        System.Collections.Generic.List<ServiceIISClient.WorkingTaskService.WorkingTask> GetAllActiveTasks(string databasePrefix);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWorkingTaskContract/ServiceStatusTestCheck", ReplyAction="http://tempuri.org/IWorkingTaskContract/ServiceStatusTestCheckResponse")]
        bool ServiceStatusTestCheck();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWorkingTaskContract/CollectionTest", ReplyAction="http://tempuri.org/IWorkingTaskContract/CollectionTestResponse")]
        System.Collections.Generic.List<ServiceIISClient.WorkingTaskService.WorkingTask> CollectionTest();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWorkingTaskContract/TestOperationGetTypes", ReplyAction="http://tempuri.org/IWorkingTaskContract/TestOperationGetTypesResponse")]
        System.Collections.Generic.List<ServiceIISClient.WorkingTaskService.WorkingTaskType> TestOperationGetTypes();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IWorkingTaskContractChannel : ServiceIISClient.WorkingTaskService.IWorkingTaskContract, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WorkingTaskContractClient : System.ServiceModel.ClientBase<ServiceIISClient.WorkingTaskService.IWorkingTaskContract>, ServiceIISClient.WorkingTaskService.IWorkingTaskContract {
        
        public WorkingTaskContractClient() {
        }
        
        public WorkingTaskContractClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WorkingTaskContractClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WorkingTaskContractClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WorkingTaskContractClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Collections.Generic.List<ServiceIISClient.WorkingTaskService.WorkingTask> GetAllTasks(string databasePrefix) {
            return base.Channel.GetAllTasks(databasePrefix);
        }
        
        public System.Collections.Generic.List<ServiceIISClient.WorkingTaskService.WorkingTask> GetAllActiveTasks(string databasePrefix) {
            return base.Channel.GetAllActiveTasks(databasePrefix);
        }
        
        public bool ServiceStatusTestCheck() {
            return base.Channel.ServiceStatusTestCheck();
        }
        
        public System.Collections.Generic.List<ServiceIISClient.WorkingTaskService.WorkingTask> CollectionTest() {
            return base.Channel.CollectionTest();
        }
        
        public System.Collections.Generic.List<ServiceIISClient.WorkingTaskService.WorkingTaskType> TestOperationGetTypes() {
            return base.Channel.TestOperationGetTypes();
        }
    }
}
