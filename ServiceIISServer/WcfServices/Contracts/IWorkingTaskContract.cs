﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using WcfDal.Models;

namespace WcfServices.Contracts
{
    [ServiceContract]
    public interface IWorkingTaskContract
    {
        [OperationContract]
        List<WorkingTask> GetAllTasks(string databasePrefix);
 
        [OperationContract]
        List<WorkingTask> GetAllActiveTasks(string databasePrefix);

        [OperationContract]
        bool ServiceStatusTestCheck();
        
        [OperationContract]
        List<WorkingTask> CollectionTest();

        [OperationContract]
        List<WorkingTaskType> TestOperationGetTypes();
    }
}
