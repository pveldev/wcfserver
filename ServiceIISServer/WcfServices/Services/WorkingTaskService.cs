﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using WcfDal.Models;
using WcfDal.Repositories;
using WcfServices.Contracts;

namespace WcfServices.Services
{
    public class WorkingTaskService : IWorkingTaskContract
    {
        //private readonly GenericRepo<WorkingTask> _genericRepo;
        private readonly WorkingTaskRepo _repo;
        private readonly WorkingTaskTypeRepo _typeRepo;

        //public WorkingTaskService(GenericRepo<WorkingTask> genericRepo, WorkingTaskRepo repo)
        //{
        //    this._genericRepo = genericRepo;
        //    this._repo = repo;
        //}

        public WorkingTaskService(WorkingTaskRepo repo, WorkingTaskTypeRepo typeRepo)
        {
            this._repo = repo;
            this._typeRepo = typeRepo;
        }

        public List<WorkingTask> CollectionTest() => new List<WorkingTask>() 
        {
            new WorkingTask(){ Id = 1, CreationDate = DateTime.Now, Name = "t", Number = 133, PriorityLevel = 3},
            new WorkingTask(){ Id = 2, CreationDate = DateTime.Now, Name = "tt", Number = 1338, PriorityLevel = 4}
        };
        public List<WorkingTask> GetAllActiveTasks(string databasePrefix)
        {
            try
            {
                var tmp = _repo.GetAllActiveTasks(databasePrefix);
                tmp.ForEach(t => t.Type = null);

                return tmp;
            }
            catch (Exception ex)
            {
                return new List<WorkingTask>();
            }
        }

        //public List<WorkingTask> GetAllTasks(string databasePrefix) => _genericRepo.GetEntities(databasePrefix);
        public List<WorkingTask> GetAllTasks(string databasePrefix) => _repo.GetAllTasks(databasePrefix);

        public bool ServiceStatusTestCheck() => true;

        public List<WorkingTaskType> TestOperationGetTypes()
        {
            var tmp = _typeRepo.GetAllWorkingTaskTypes("second").ToList();
            return tmp;
        }
    }  
}
