﻿using Quartz;
using System.Diagnostics;
using System.Threading.Tasks;

namespace WcfServices.QuartzJobs
{
    public class ConsoleSpamerJob : IJob
    {
        Task IJob.Execute(IJobExecutionContext context) =>
            Task.Run(() => Debug.WriteLine($"[{context.FireTimeUtc.LocalDateTime}] Hello, QUARTZ! [in {context.JobRunTime}]"));
    }

    public class ConsoleSecondSpamerJob : IJob
    {
        Task IJob.Execute(IJobExecutionContext context) =>
            Task.Run(() => Debug.WriteLine($"[{context.FireTimeUtc.LocalDateTime}] Hello, QUARTZ! [in {context.JobRunTime}]"));
    }
}
