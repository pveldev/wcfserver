﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WcfServices.QuartzJobs;

namespace App_Code
{
    public class QuartzScheduler
    {
        readonly IScheduler _scheduler;
        readonly ISchedulerFactory _schedulerFactory;

        public QuartzScheduler()
        {
            _schedulerFactory = new StdSchedulerFactory();
            _scheduler = _schedulerFactory.GetScheduler().Result;
            _scheduler.Start();

            //Проверка работы в целом
            StartConsoleSpamerJob();
        }

        public void StartConsoleSpamerJob() //Эта штука должны быть сервисом? Синглтон который?
        {
            //ITrigger everySecondTrigger = TriggerBuilder.Create()
            //    .WithIdentity("EverySecond_Forever", "mainGroup")
            //    .WithSimpleSchedule(x => x.WithIntervalInSeconds(3).RepeatForever())
            //    .Build();

            ITrigger everyDayTrigger = TriggerBuilder.Create()
               .WithDailyTimeIntervalSchedule
                (s =>
                    s.WithIntervalInHours(24)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(9, 10))
                    .InTimeZone(TimeZoneInfo.Local)
                 )
               .Build();

            IJobDetail goodMorningJob = JobBuilder.Create<ConsoleSpamerJob>()
                    .WithIdentity("consoleSpamerJob", "mainGroup")
                    .Build();

            _scheduler.ScheduleJob(goodMorningJob, everyDayTrigger);
        }
        public void StartManyConsoleSpamerJob()
        {
            List<ITrigger> triggers = new List<ITrigger>() 
            { 
                TriggerBuilder.Create()
                    .WithIdentity("EverySecond_Forever", "mainGroup")
                    .WithSimpleSchedule(x => x.WithIntervalInSeconds(3).RepeatForever())
                    .Build()
            };

            IJobDetail job = JobBuilder.Create<ConsoleSpamerJob>()
                    .WithIdentity("consoleSpamerJob", "mainGroup")
                    .Build();

            IJobDetail secondJob = JobBuilder.Create<ConsoleSecondSpamerJob>()
                  .WithIdentity("consoleSecondSpamerJob", "mainGroup")
                  .Build();

            var triggersAndJobs = new Dictionary<IJobDetail, IReadOnlyCollection<ITrigger>>
            {
                { job, triggers },
                { secondJob, triggers }
            };

            _scheduler.ScheduleJobs(triggersAndJobs, true);
        }
    }
}

