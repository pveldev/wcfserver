﻿using Autofac;
using Autofac.Integration.Wcf;
using WcfServices.Services;
using WcfDal.Contexts;
using WcfDal.Repositories;
using WcfDal.Contexts.Implementations;

namespace App_Code
{

    //RegisterInstance - инициадизация, при помещении в контейнер.
    //RegisterType - самое обычно помещение в контейнер
    //.ExternallyOwned - самостоятельный контроль времени жизни (не дает autofac вызывать Dispose)
    public static class Container
    {
        static readonly ContainerBuilder _builder = new ContainerBuilder();

        public static void Configure()
	    {
            //Сиглетные контексты через фабрику с зависимостями
            _builder.RegisterType<WorkingTaskManagerMainContext>();
            _builder.RegisterType<WorkingTaskManagerSecondContext>();
            _builder.RegisterType(typeof(WorkingTaskManagerContextFactory)).SingleInstance();
            _builder.RegisterInstance(new QuartzScheduler());

            //repos
            //_builder.RegisterType<GenericRepo<IBaseModel>>();
            _builder.RegisterType<WorkingTaskRepo>();
            _builder.RegisterType<WorkingTaskTypeRepo>();

            //services
            _builder.RegisterType<WorkingTaskService>();

            AutofacHostFactory.Container = _builder.Build();
        }
    }
}