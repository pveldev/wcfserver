﻿<%@ Application Language="C#" %>
<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        //https://riptutorial.com/wcf/example/19594/how-to-configure-a-wcf-service-to-use-a-dependency-injection-container--castle-windsor-
        //https://autofac.org/

        // WCF integration docs are at:
        // https://autofac.readthedocs.io/en/latest/integration/wcf.html

        App_Code.Container.Configure();
    }

</script>
