﻿using System.Data.Entity;
using WcfDal.Contexts;
using System.Linq;
using WcfDal.Models.Core;
using System.Collections.Generic;
using WcfDal.Constants;

namespace WcfDal.Repositories
{
    /// <summary>
    /// Generic CRUD-repo
    /// </summary>
    /// <typeparam name="T">IBaseModel</typeparam>
    public class GenericRepo<T> where T: class, IBaseModel
    {
        readonly WorkingTaskManagerContextFactory _contextFactory;
        public GenericRepo(WorkingTaskManagerContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        /// <summary>
        /// Групповое добавление.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="databasePrefix"></param>
        /// <returns></returns>
        public List<T> AddRangeEntities(IEnumerable<T> entities, string databasePrefix = DbConstants.MainDbPrefix) => 
            (_contextFactory.GetTaskManagerContext(databasePrefix) as DbContext).Set<T>().AddRange(entities).ToList();

        /// <summary>
        /// Insert or Update entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="databasePrefix"></param>
        /// <returns></returns>
        public bool UpsertEntity(T entity, string databasePrefix = DbConstants.MainDbPrefix)
        {
            try
            {
                var context = (DbContext)_contextFactory.GetTaskManagerContext(databasePrefix);
                
                context.Entry<T>(entity).State = entity.Id == 0 ? EntityState.Added : EntityState.Modified;
                context.SaveChanges();

                return true;
            }
            catch (System.Exception ex)
            {
                //todo: ex processing
                return false;
            }
        }

        public List<T> GetEntities(string databasePrefix = DbConstants.MainDbPrefix) => 
            (_contextFactory.GetTaskManagerContext(databasePrefix) as DbContext).Set<T>().ToList();
        
        /// <summary>
        /// Получение одной записи через .FirstOrDefault()
        /// </summary>
        /// <param name="entityId">Id сущности</param>
        /// <param name="databasePrefix">Префикс базы данных</param>
        /// <returns></returns>
        public T GetEntityOrNullById(int entityId, string databasePrefix = DbConstants.MainDbPrefix) =>
                (_contextFactory.GetTaskManagerContext(databasePrefix) as DbContext).Set<T>().FirstOrDefault(entity => entity.Id == entityId);
    }
}
