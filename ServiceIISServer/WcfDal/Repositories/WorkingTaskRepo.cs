﻿using System.Collections.Generic;
using System.Linq;
using WcfDal.Contexts;
using WcfDal.Models;

namespace WcfDal.Repositories
{
    public class WorkingTaskRepo
    {
        readonly WorkingTaskManagerContextFactory _contextFactory;

        public WorkingTaskRepo(WorkingTaskManagerContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public List<WorkingTask> GetAllTasks(string databasePrefix) => _contextFactory.GetTaskManagerContext(databasePrefix).WorkingTasks.ToList();
        public List<WorkingTask> GetAllActiveTasks(string databasePrefix) => 
            _contextFactory.GetTaskManagerContext(databasePrefix)
            .WorkingTasks
            //.AsNoTracking()
            //.Include("Type")
            .Where(task => !task.IsCompleted && !task.IsArchive)
            .ToList();
    }
}
