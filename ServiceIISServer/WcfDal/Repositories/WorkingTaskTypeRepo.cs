﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfDal.Contexts;
using WcfDal.Models;

namespace WcfDal.Repositories
{
    public class WorkingTaskTypeRepo
    {
        readonly WorkingTaskManagerContextFactory _contextFactory;

        public WorkingTaskTypeRepo(WorkingTaskManagerContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public List<WorkingTaskType> GetAllWorkingTaskTypes(string databasePrefix) => _contextFactory.GetTaskManagerContext(databasePrefix).TaskTypes.ToList();
        
        //попробовать типы перегонять в клиент
        //почитать про ноунТайпы
        //https://stackoverflow.com/questions/3167932/when-is-it-appropriate-to-use-the-knowntype-attribute
    }
}
