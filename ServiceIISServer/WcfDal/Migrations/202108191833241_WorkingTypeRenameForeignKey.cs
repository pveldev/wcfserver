﻿namespace WcfDal.Migrations.TmSecond
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WorkingTypeRenameForeignKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.WorkingTasks", "Type_Id", "dbo.WorkingTaskTypes");
            DropIndex("dbo.WorkingTasks", new[] { "Type_Id" });
            AlterColumn("dbo.WorkingTasks", "Type_Id", c => c.Int());
            CreateIndex("dbo.WorkingTasks", "Type_Id");
            AddForeignKey("dbo.WorkingTasks", "Type_Id", "dbo.WorkingTaskTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkingTasks", "Type_Id", "dbo.WorkingTaskTypes");
            DropIndex("dbo.WorkingTasks", new[] { "Type_Id" });
            AlterColumn("dbo.WorkingTasks", "Type_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.WorkingTasks", "Type_Id");
            AddForeignKey("dbo.WorkingTasks", "Type_Id", "dbo.WorkingTaskTypes", "Id", cascadeDelete: true);
        }
    }
}
