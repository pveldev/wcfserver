﻿namespace WcfDal.Migrations.TmSecond
{
    using System.Data.Entity.Migrations;

    public partial class AddFakeTasks : DbMigration
    {
        public override void Up()
        {
            Sql($@"
INSERT INTO Tasks VALUES 
(0,0,1,'Погладить кота','от головы к хвосту',GETDATE(),3,13371)
,(0,0,2,'Покормить кота','',GETDATE(),3,13372)
,(0,0,2,'Выбрать новое дополнение для манчкина','',GETDATE(),3,13373)
");
        }
        
        public override void Down()
        {
        }
    }
}
