﻿namespace WcfDal.Migrations.TmSecond
{
    using System.Data.Entity.Migrations;

    public partial class UpdateAndAddDataToTaskTypeTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskTypes", "Code", c => c.String(nullable: false, maxLength: 3));
            Sql($@"
                    INSERT INTO TaskTypes VALUES 
                     ('Заявка', 'Любая типовая задача',                 GETDATE(), '101')
                    ,('Тикет',  'Назначенный пользовательский тикет',   GETDATE(), '102')
                    ,('Прочее', 'Тип-свалка для всего неопознанного',   GETDATE(), '103')
                ");
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskTypes", "Code");
        }
    }
}
