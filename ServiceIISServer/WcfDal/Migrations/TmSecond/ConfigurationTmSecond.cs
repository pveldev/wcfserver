﻿namespace WcfDal.Migrations.TmSecond
{
    using System.Data.Entity.Migrations;

    internal sealed class ConfigurationTmSecond : DbMigrationsConfiguration<WcfDal.Contexts.Implementations.WorkingTaskManagerSecondContext>
    {
        public ConfigurationTmSecond()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WcfDal.Contexts.Implementations.WorkingTaskManagerSecondContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}
