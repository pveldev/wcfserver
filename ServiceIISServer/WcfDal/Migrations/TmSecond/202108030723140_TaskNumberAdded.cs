﻿namespace WcfDal.Migrations.TmSecond
{
    using System.Data.Entity.Migrations;

    public partial class TaskNumberAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tasks", "Number", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tasks", "Number");
        }
    }
}
