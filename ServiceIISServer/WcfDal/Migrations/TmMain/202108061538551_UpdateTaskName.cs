﻿using System.Data.Entity.Migrations;

namespace WcfDal.Migrations.TmMain
{
    public partial class UpdateTaskName : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Tasks", newName: "WorkingTasks");
            RenameTable(name: "dbo.TaskTypes", newName: "WorkingTaskTypes");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.WorkingTaskTypes", newName: "TaskTypes");
            RenameTable(name: "dbo.WorkingTasks", newName: "Tasks");
        }
    }
}
