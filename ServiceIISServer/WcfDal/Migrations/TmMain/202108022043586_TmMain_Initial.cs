﻿namespace WcfDal.Migrations.TmMain
{
    using System.Data.Entity.Migrations;

    public partial class TmMain_Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsArchive = c.Boolean(nullable: false),
                        IsCompleted = c.Boolean(nullable: false),
                        PriorityLevel = c.Int(nullable: false),
                        Name = c.String(maxLength: 300),
                        Description = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        Type_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TaskTypes", t => t.Type_Id, cascadeDelete: true)
                .Index(t => t.Type_Id);
            
            CreateTable(
                "dbo.TaskTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 300),
                        Description = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tasks", "Type_Id", "dbo.TaskTypes");
            DropIndex("dbo.Tasks", new[] { "Type_Id" });
            DropTable("dbo.TaskTypes");
            DropTable("dbo.Tasks");
        }
    }
}
