﻿namespace WcfDal.Migrations.TmMain
{
    using System.Data.Entity.Migrations;

    internal sealed class ConfigurationTmMain : DbMigrationsConfiguration<WcfDal.Contexts.Implementations.WorkingTaskManagerMainContext>
    {
        public ConfigurationTmMain()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WcfDal.Contexts.Implementations.WorkingTaskManagerMainContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}
