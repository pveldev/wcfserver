﻿namespace WcfDal.Constants
{
    public static class DbConstants
    {
        /// <summary>
        /// Максимальное количество символов для полей имен и названий.
        /// </summary>
        public const int NameMaxSymbols = 300;

        /// <summary>
        /// Параметр-префикс для фабрики баз данных
        /// </summary>
        public const string MainDbPrefix = "main";

        /// <summary>
        /// Параметр-префикс для фабрики баз данных
        /// </summary>
        public const string SecondDbPrefix = "second";
    }
}
