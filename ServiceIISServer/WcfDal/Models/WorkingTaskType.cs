﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using WcfDal.Constants;
using WcfDal.Models.Core;

namespace WcfDal.Models
{
    [DataContract]
    public class WorkingTaskType //: IBaseModel, ITypeTable
    {
        public WorkingTaskType()
        {
            WorkingTasks = new HashSet<WorkingTask>();
        }

        [Key]
        //[DataMember]
        public int Id { get; set; }

        [StringLength(DbConstants.NameMaxSymbols)]
        [DataMember]
        public string Name { get; set; }

        //[DataMember]
        public string Description { get; set; }

        [DataType("DateTime2")]
        [Required]
        //[DataMember]
        public DateTime CreationDate { get; set; }


        [Required, StringLength(3), DataMember]
        public string Code { get; set; }

        //[DataMember]
        [IgnoreDataMember]
        public virtual ICollection<WorkingTask> WorkingTasks{ get; private set; }
    }
}