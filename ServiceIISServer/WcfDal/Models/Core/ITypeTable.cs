﻿using System.Runtime.Serialization;

namespace WcfDal.Models.Core
{
    public interface ITypeTable
    {
        /// <summary>
        /// Строковый код, являющийся сквозным между разным базами.
        /// </summary>
        [DataMember]
        string Code { get; set; }
    }
}
