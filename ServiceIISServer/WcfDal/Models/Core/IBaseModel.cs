﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using WcfDal.Constants;

namespace WcfDal.Models.Core
{
    /// <summary>
    /// Интерфейс для всех моделей базы данных
    /// </summary>
    public interface IBaseModel
    {
        [Key]
        //[DataMember]
        int Id { get; set; }

        [StringLength(DbConstants.NameMaxSymbols)]
        //[DataMember]
        string Name { get; set; }

       // [DataMember]
        string Description { get; set; }

        [DataType("DateTime2")]
        //[DataMember]
        [Required]
        DateTime CreationDate { get; set; }
    }
}
