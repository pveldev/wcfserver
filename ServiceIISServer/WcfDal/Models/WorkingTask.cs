﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using WcfDal.Constants;
using WcfDal.Models.Core;

namespace WcfDal.Models
{
    [DataContract]
    public class WorkingTask //: IBaseModel
    {
        [Key]
        //[DataMember]
        public int Id { get; set; }

        [StringLength(DbConstants.NameMaxSymbols)]
        [DataMember]
        public string Name { get; set; }

        //[DataMember]
        public string Description { get; set; }

        [DataType("DateTime2")]
        [Required]
        //[DataMember]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Порядковый номер задачи.
        /// </summary>
       // [DataMember]
        public int Number { get; set; }

        /// <summary>
        /// Флаг архивирована/активна.
        /// </summary>
        //[DataMember]
        public bool IsArchive { get; set; }

        //todo: заменить на статусы
        /// <summary>
        /// Флаг готовности.
        /// </summary>
        //[DataMember]
        public bool IsCompleted { get; set; }

        /// <summary>
        /// Приоритет задачи.
        /// </summary>
        [Required]
        //[DataMember]
        public int PriorityLevel { get; set; }

        /// <summary>
        /// Тип задачи.
        /// </summary>
        //[Required]
        //[DataMember]
        [IgnoreDataMember]
        public virtual WorkingTaskType Type { get; set; }
    }
}
