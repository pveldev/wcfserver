﻿using System.Data.Entity;
using WcfDal.Contexts.Abstractions;
using WcfDal.Models;

namespace WcfDal.Contexts.Implementations
{
    public class WorkingTaskManagerSecondContext : DbContext, IWorkingTaskManagerContext
    {
        public WorkingTaskManagerSecondContext()
        :base("name=TaskManagerSecondContext")
        {

        }
        public DbSet<WorkingTask> WorkingTasks { get; set; }
        public DbSet<WorkingTaskType> TaskTypes { get; set; }
    }
}
