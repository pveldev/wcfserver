﻿using System.Data.Entity;
using WcfDal.Contexts.Abstractions;
using WcfDal.Models;

namespace WcfDal.Contexts.Implementations
{
    public class WorkingTaskManagerMainContext : DbContext, IWorkingTaskManagerContext
    {
        public WorkingTaskManagerMainContext()
        :base("name=TaskManagerMainContext")
        {

        }

        public DbSet<Models.WorkingTask> WorkingTasks { get; set; }
        public DbSet<WorkingTaskType> TaskTypes { get; set; }
    }
}
