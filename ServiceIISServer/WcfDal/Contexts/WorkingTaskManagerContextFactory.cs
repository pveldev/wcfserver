﻿using WcfDal.Contexts.Abstractions;
using WcfDal.Contexts.Implementations;

namespace WcfDal.Contexts
{
    public class WorkingTaskManagerContextFactory
    {
        private readonly WorkingTaskManagerMainContext _mainContext;
        private readonly WorkingTaskManagerSecondContext _secondContext;

        public WorkingTaskManagerContextFactory(WorkingTaskManagerMainContext mainContext, WorkingTaskManagerSecondContext secondContext)
        {
            _mainContext = mainContext;
            _secondContext = secondContext;
        }

        public IWorkingTaskManagerContext GetTaskManagerContext(string databasePrefix)
        {
            switch (databasePrefix.ToLower())
            {
                case "second":
                case "test":
                case "":
                    return _secondContext;
                case "main":
                case "master":
                default:
                    return _mainContext;
            }
        }
    }
}

