﻿using System.Data.Entity;
using WcfDal.Models;

namespace WcfDal.Contexts.Abstractions
{
    public interface IWorkingTaskManagerContext
    {
        DbSet<WorkingTask> WorkingTasks { get; set; }
        DbSet<WorkingTaskType> TaskTypes { get; set; }
    }
}
